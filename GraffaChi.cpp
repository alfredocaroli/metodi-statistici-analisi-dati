void GraffaChi() {
  double B[6] = {15, 16, 17, 18, 19, 20};
  double chi[6] = {9.36, 5.68, 3.41, 2.08, 1.35, 0.97};

  auto g = new TGraph(6, B, chi);

  g->SetMarkerStyle(kFullCircle);
  g->SetMarkerColor(kRed);
  g->SetLineWidth(2);

  g->SetTitle("Andamento Chi quadro ridotto");
  g->GetXaxis()->SetTitle("Taglio su B");
  g->GetYaxis()->SetTitle("Chi quadro / NDF");
  
  g->Draw("ALP");
  
}
