void Simula_Integra(){
  double N_acc = 319897;
  double entries_S = 1.45135e+05;
  double entries_B = 1-15678e+05;
  //////////////////////////////////////////////////////////////////////////
  TH1F *hist_S = new TH1F("hist_A", "Istogramma A", sqrt(entries_S), 0, 12);
  TH1F *hist_B = new TH1F("hist_A", "Istogramma A", sqrt(entries_B), 0, 12);

  int seed_S = 0;
  TRandom2 *rand_S = new TRandom2(seed_S);
  
  for(int i = 0; i < entries_S; i++) {
    double r_S = rand_S->Gaus(2.57429e+00, 3.05892e-01);
    hist_S->Fill(r_S);
  }
  double integral_error_S;
  double integral_S = hist_S->IntegralAndError(0, sqrt(entries_S)+1, integral_error_S, "w");
  cout << "Integrale S: " << integral_S << endl;
  cout << "Errore S: " << integral_error_S << endl;

  int seed_B = 1;
  TRandom2 *rand_B = new TRandom2(seed_B);
  
  for(int i = 0; i < entries_B; i++) {
    double r_B = rand_B->Exp(1.0/(6.46173e-01));
    hist_B->Fill(r_B);
  }
  double integral_error_B;
  double integral_B = hist_B->IntegralAndError(0, sqrt(entries_B)+1, integral_error_B, "w");
  cout << "Integrale B: " << integral_B << endl;
  cout << "Errore B: " << integral_error_B << endl;
  
  TCanvas *c1 = new TCanvas();
  hist_S->SetFillColor(kBlue-9);
  hist_S->SetStats(0);
  hist_S->Draw();
  hist_B->SetFillColor(kRed-9);
  hist_B->SetStats(0);
  hist_B->Draw("SAME");

  ///calcola tutto///
  
  double int_S_tot = 201944;
  double int_S_tot_err = 449;
  double int_B_tot = 353627;
  double int_B_tot_err = 595;
  double N_tot = 551127;
  double N_tot_err = sqrt(N_tot);
  double N_acc_err = sqrt(N_acc);
  
  double eff_S = integral_S / int_S_tot; 
  double eff_S_err = sqrt( pow((integral_error_S / int_S_tot), 2) +
			   pow(int_S_tot_err * integral_S / int_S_tot / int_S_tot, 2) );
  
  double eff_B = integral_B / int_B_tot;
  double eff_B_err = sqrt( pow((integral_error_B / int_B_tot), 2) +
			   pow(int_B_tot_err * integral_B / int_B_tot / int_B_tot, 2) );

  double N_e = (N_acc - (eff_B * N_tot)) / (eff_S - eff_B);
  double der_risp_N_acc = 1 / (eff_S - eff_B);
  double der_risp_N_tot = -eff_B / (eff_S - eff_B);
  double der_risp_eff_B = (-N_tot * (eff_S - eff_B) + N_acc - (eff_B * N_tot)) / pow((eff_S - eff_B), 2);
  double der_risp_eff_S = -(N_acc - (eff_B * N_tot)) / pow((eff_S - eff_B), 2);
  double N_e_err = sqrt( pow(der_risp_N_acc * N_acc_err, 2) +
			 pow(der_risp_N_tot * N_tot_err, 2) + 
			 pow(der_risp_eff_B * eff_B_err, 2) +
			 pow(der_risp_eff_S * eff_S_err, 2) );
  
  cout << "Numero tot " << N_tot << " con errore " << N_tot_err << endl;
  cout << "Numero acc " << N_acc << " con errore " << N_acc_err << endl;
  cout << "Efficienza segnale " << eff_S << " con errore " << eff_S_err << endl;
  cout << "Efficienza background " << eff_B << " con errore " << eff_B_err << endl;
  cout << "---------------------------------------------------" << endl;
  cout << "---------------------------------------------------" << endl;
  cout << "Numero elettroni stimato: " << N_e << endl;
  cout << "Errore stimato: " << N_e_err << endl;
  cout << "Errore relativo (da plottare): " << N_e_err / N_e << endl;
}
