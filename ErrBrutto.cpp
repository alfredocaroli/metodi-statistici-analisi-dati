void ErrBrutto() {
  double errRel[7] = {8.08437e+02 / 1.65785e+05,
		      6.99493e+02 / 1.33888e+05,
		      7.37852e+02 / 1.45135e+05,
		      2.27182e+02 / 3.72853e+03,
		      2.09824e+02 / 7.76481e+03,
		      1.93707e+02 / 1.10922e+04,
		      1.79471e+02 / 1.35846e+04};
  double B[7] = {5, 6, 7, 8, 9, 10, 11};

  auto g = new TGraph(7, B,errRel);

  g->SetMarkerStyle(kFullCircle);
  g->SetMarkerColor(kRed);
  g->SetLineWidth(2);

  g->SetTitle("Errore relativo TMinuit");
  g->GetXaxis()->SetTitle("Taglio su B");
  g->GetYaxis()->SetTitle("Err. relativo");
  
  g->Draw("AL*");
  
}
