void ErrRelativo() {
  double errRel[7] = {0.0183036, 0.0178332, 0.0106438, 0.00773135, 0.00918125, 0.011873, 0.017897};
  double B[7] = {5, 6, 7, 8, 9, 10, 11};

  auto g = new TGraph(7, B,errRel);

  g->SetMarkerStyle(kFullCircle);
  g->SetMarkerColor(kRed);
  g->SetLineWidth(2);

  g->SetTitle("Andamento errore relativo");
  g->GetXaxis()->SetTitle("Taglio su B");
  g->GetYaxis()->SetTitle("Err. relativo");
  
  g->Draw("AL*");
  
}
