double fitfun(double *x, double *par){
  double fitval = par[0] * exp(par[1] * x[0]) +
    par[2] / sqrt(2.0 * TMath::Pi()) / par[4] * exp(-(x[0]-par[3])*(x[0]-par[3])/2./par[4]/par[4]);

  return fitval;
}

void LeggiTree(){

  TF1 *func = new TF1("fitfun", fitfun,
		      0, 12, //intervallo
		      5);    //numero di parametri
  
  func->SetParNames("cost1", "slope", "cost2", "mu", "sigma");
  func->SetParameter(0, 7.57844e+03);
  func->SetParameter(1, -6.46173e-01);
  func->SetParameter(2, 2.58916e+02);
  
  func->SetParameter(3, 2.57429e+00);
  func->SetParLimits(3, 2, 3);
  
  func->SetParameter(4, 3.05892e-01);

  TFile *input = new TFile("tree_dati.root", "read");
  TTree *tree = (TTree*)input->Get("tree");

  double A, B;
  tree->SetBranchAddress("A", &A);
  tree->SetBranchAddress("B", &B);

  int entries = tree->GetEntries();
  TH1F *hist_A = new TH1F("hist_A", "Istogramma A", 1000, 0, 12);
  
  for(int i = 0; i < entries; i++) {
    tree->GetEntry(i);
    hist_A->Fill(A);
  }
  double integral_error;
  double integral = hist_A->IntegralAndError(0, 1001, integral_error, "w");
  cout << "Integrale: " << integral << endl;
  cout << "Errore: " << integral_error << endl;
  
  TCanvas *c1 = new TCanvas();
  hist_A->SetFillColor(kGreen-9);
  hist_A->GetXaxis()->SetTitle("A (arbitrary units)");
  hist_A->GetYaxis()->SetTitle("Entries");
  //hist_A->SetStats(0);
  hist_A->Fit(func, "R");
}
