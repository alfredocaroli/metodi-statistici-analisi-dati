double cost1 = 7.57844e+03;
double slope = -6.46173e-01;
double cost2 = 2.58916e+02;
double mean = 2.57429e+00;
double sigma = 3.05892e-01;
double segnale, segnale_err, back, back_err;

double g_background(Double_t* x) {
  return exp(slope * x[0]);
}

double g_signal(Double_t* x) {
  return 1.0 / sqrt(2.0 * TMath::Pi()) / sigma * exp(-(x[0]-mean)*(x[0]-mean)/2./sigma/sigma);
}

int npar = 2;
vector<double>* xVecPtr = new vector<double>(); //global pointer to data

//read data from tree in root file

void getData(double valore) {
  TFile *input = new TFile("tree_dati.root", "read");
  TTree *tree = (TTree*)input->Get("tree");
  double A, B;
  tree->SetBranchAddress("A", &A);
  tree->SetBranchAddress("B", &B);
  int entries = tree->GetEntries();
  for(int i = 0; i < entries; i++){
    tree->GetEntry(i);
    if(B >= valore) xVecPtr->push_back(A);
  }
  cout << "Entries: " << xVecPtr->size() << endl;
}

//fcn passes back f = -ln(L), the function to be minimized.

void fcn(int &npar, double* deriv, double& f, double par[], int flag) {
  vector<double> xVec = *xVecPtr; //xVecPtr is global
  int nev = xVec.size();
  double f1;
  double lnL = 0.0;
  for(int i = 0; i < nev; i++){
    double x = xVec[i];
    f1 = ( par[0]*g_signal(&x) + par[1]*g_background(&x) ); //probability for that event
    lnL += log(f1); //need positive f
  }
  lnL -= par[0] + par[1];
  f = -lnL;
}

void Minimizza(double soglia) {
  getData(soglia);
  TMinuit minuit(npar);
  minuit.SetFCN(fcn);
  minuit.SetPrintLevel(0);

  double par[npar], stepSize[npar], minVal[npar], maxVal[npar];
  string parName[npar];
  par[0] = 1.0;
  stepSize[0] = 0.1;
  minVal[0] = 0;
  maxVal[0] = 0;
  parName[0] = "signal";

  par[1] = 1.0;
  stepSize[1] = 0.1;
  minVal[1] = 0;
  maxVal[1] = 0;
  parName[1] = "background";

  for(int i = 0; i<npar; i++) {
    minuit.DefineParameter(i, parName[i].c_str(),
	                   par[i], stepSize[i], minVal[i], maxVal[i]);
  }

  minuit.SetErrorDef(0.5);
  minuit.SetPrintLevel(0);
  minuit.Migrad();
  minuit.GetParameter(0, segnale, segnale_err);
  minuit.GetParameter(1, back, back_err);
  
  ////////////////FINE LIKELIHOOD/////////////////////////////////////////
  
  double N_acc = xVecPtr->size();
  double entries_S = segnale;
  double entries_B = back;
  
  TH1F *hist_S = new TH1F("hist_S", "Eventi generati (B>=5)", sqrt(entries_S), 0, 12);
  TH1F *hist_B = new TH1F("hist_B", "Eventi generati (B>=5)", sqrt(entries_B), 0, 12);

  int seed_S = 0;
  TRandom2 *rand_S = new TRandom2(seed_S);

  for(int i = 0; i < entries_S; i++) {
    double r_S = rand_S->Gaus(2.57429e+00, 3.05892e-01);
    hist_S->Fill(r_S);
  }
  double integral_error_S;
  double integral_S = hist_S->IntegralAndError(0, sqrt(entries_S)+1, integral_error_S, "w");
  cout << "Integrale elettroni: " << integral_S << endl;
  cout << "Errore integrale elettroni: " << integral_error_S << endl;

  int seed_B = 1;
  TRandom2 *rand_B = new TRandom2(seed_B);

  for(int i = 0; i < entries_B; i++) {
    double r_B = rand_B->Exp(1.0/(6.46173e-01));
    hist_B->Fill(r_B);
  }
  double integral_error_B;
  double integral_B = hist_B->IntegralAndError(0, sqrt(entries_B)+1, integral_error_B, "w");
  cout << "Integrale protoni: " << integral_B << endl;
  cout << "Errore integrale protoni: " << integral_error_B << endl;

  cout << "Larghezza bin segnale: " << hist_S->GetXaxis()->GetBinWidth(100) << endl;
  cout << "Larghezza bin background: " << hist_B->GetXaxis()->GetBinWidth(100) << endl;
  //TCanvas *c1 = new TCanvas();
  hist_S->GetXaxis()->SetTitle("A (arbitrary units)");
  hist_S->GetYaxis()->SetTitle("Entries");
  hist_S->SetFillColor(kBlue-9);
  hist_S->SetStats(0);
  //hist_S->Draw();
  hist_B->SetFillColor(kRed-9);
  hist_B->SetStats(0);
  //hist_B->Draw("SAME");

  ///calcola tutto///

  double int_S_tot = 201944.0;
  double int_S_tot_err = 449.0;
  double int_B_tot = 353627.0;
  double int_B_tot_err = 595.0;
  double N_tot = 551127.0;
  double N_tot_err = sqrt(N_tot);
  double N_acc_err = sqrt(N_acc);

  double eff_S = integral_S / int_S_tot;
  double eff_S_err = sqrt( pow((integral_error_S / int_S_tot), 2) +
                           pow(int_S_tot_err * integral_S / int_S_tot / int_S_tot, 2) );

  double eff_B = integral_B / int_B_tot;
  double eff_B_err = sqrt( pow((integral_error_B / int_B_tot), 2) +
                           pow(int_B_tot_err * integral_B / int_B_tot / int_B_tot, 2) );

  double N_e = (N_acc - (eff_B * N_tot)) / (eff_S - eff_B);
  double der_risp_N_acc = 1.0 / (eff_S - eff_B);
  double der_risp_N_tot = -eff_B / (eff_S - eff_B);
  double der_risp_eff_B = (-N_tot * (eff_S - eff_B) + N_acc - (eff_B * N_tot)) / pow((eff_S - eff_B), 2)\
;
  double der_risp_eff_S = -(N_acc - (eff_B * N_tot)) / pow((eff_S - eff_B), 2);
  double N_e_err = sqrt( pow(der_risp_N_acc * N_acc_err, 2) +
                         pow(der_risp_N_tot * N_tot_err, 2) +
                         pow(der_risp_eff_B * eff_B_err, 2) +
                         pow(der_risp_eff_S * eff_S_err, 2) );

  cout << "Numero tot " << N_tot << " con errore " << N_tot_err << endl;
  cout << "Numero acc " << N_acc << " con errore " << N_acc_err << endl;
  cout << "Efficienza segnale " << eff_S << " con errore " << eff_S_err << endl;
  cout << "Efficienza background " << eff_B << " con errore " << eff_B_err << endl;
  cout << "---------------------------------------------------" << endl;
  cout << "---------------------------------------------------" << endl;
  cout << "Numero elettroni stimato: " << N_e << endl;
  cout << "Errore stimato: " << N_e_err << endl;
  cout << "Errore relativo (da plottare): " << N_e_err / N_e << endl;
}
