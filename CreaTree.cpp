void CreaTree() {
  
  fstream dati;
  dati.open("dati_caroli_alfredo.txt", ios::in);

  double A, B;
  TFile *file = new TFile("tree_dati.root", "recreate");
  TTree *tree = new TTree("tree", "Tree Metodi Statistici");

  tree->Branch("A", &A, "A/D");
  tree->Branch("B", &B, "B/D");

  while(!dati.eof()) {
    dati >> A >> B >> ws;
    tree->Fill();
  }
  file->Write();
  file->Close();
  dati.close();
  return;
}
